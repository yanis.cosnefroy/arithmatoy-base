#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  size_t len_rhs = strlen(rhs);
  size_t len_lhs = strlen(lhs);
  const char* change_value = rhs;
  unsigned int d_lhs;
  unsigned int d_rhs;
  unsigned int val = 0;
  unsigned int sum;

  if(len_lhs < len_rhs){
    rhs = lhs;
    lhs = change_value;

    len_lhs = strlen(lhs);
  	len_rhs = strlen(rhs);	
  }

  
  char *res = calloc(len_lhs + 1, sizeof(char));
  size_t i = 0;
  while(i < len_lhs){
    d_lhs = get_digit_value(lhs[len_lhs - i -1]);

    if(i < len_rhs ){
      d_rhs = get_digit_value(rhs[len_rhs - i -1]);
    }else{
      d_rhs = 0;
    }

    sum = d_lhs + d_rhs + val;
    val = sum/base;
    sum = sum%base;
    res[len_lhs - i - 1] = to_digit(sum);
    i++;
  }

  char *resp;
  if(val != 0){
  	resp = calloc(len_lhs + 2, sizeof(char));
  	resp[0] = to_digit(val);
  	strcpy(resp + 1, res);
  }
  else{
    resp = res;
  }
  return resp;
}


char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  size_t len_lhs = strlen(lhs);
  size_t len_rhs = strlen(rhs);
  unsigned int d_lhs;
  unsigned int d_rhs;
  int res_sub;
  unsigned int val = 0;


  if(len_lhs < len_rhs){
    return NULL;
  }

  char *res = calloc(len_lhs + 1, sizeof(char));

  size_t i = 0;
  while(i < len_lhs){
    d_lhs = get_digit_value(lhs[len_lhs - i - 1]);

    if(i < len_rhs ){
      d_rhs = get_digit_value(rhs[len_rhs - i -1]);
    }else{
      d_rhs = 0;
    }

    res_sub = d_lhs - d_rhs - val;

    if(res_sub < 0){
      val = 1;
      res_sub += base;
    }
    else{
      val=0;
    }

    res[len_lhs - i -1] = to_digit(res_sub);
    i++;
  }

  if (val != 0){
    return NULL;
  }

  res = (char *) drop_leading_zeros(res);
  char *resp = calloc(strlen(res) + 1, sizeof(char));
  strcpy(resp, res);

  
  return resp;

}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the res as a
  // string Implement the algorithm Return the res

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  size_t len_lhs = strlen(lhs);
  size_t len_rhs = strlen(rhs);
  const char* change_value = rhs;

  if(len_lhs < len_rhs){
    rhs = lhs;
    lhs = change_value;

    len_lhs = strlen(lhs);
  	len_rhs = strlen(rhs);	
  }


}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char change_value = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = change_value;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a res with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
