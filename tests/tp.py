# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if(n==0):
        return "0"
    else:
        return "S" * n + "0"


def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    cpt = 0
    if(a == "0"):
        return b
    if(b == "0"):
        return a
    for i in a:
        if (i == "S"):
            cpt += 1
    return "S" * cpt + b

def multiplication(a: str, b: str) -> str:
    cptA = 0
    cptB = 0
    if(a == "0" or b == "0"):
        return "0"
    for i in a:
        if(i == "S"):
            cptA += 1
    for i in b:
        if(i == "S"):
            cptB += 1 
    res = cptA * cptB
    return "S" * res + "0"


def facto_ite(n: int) -> int:
    res = 1
    i=1
    if(n == 0):
        return 1
    
    while(i != n+1):
        res = res * i
        i += 1
    return res

def facto_rec(n: int) -> int:
    if(n == 0):
        return 1
    else:
        return n * facto_rec(n-1)


def fibo_rec(n: int) -> int:
    if(n == 0):
        return 0
    elif(n == 1):
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    i = 1
    a = 0
    b = 1

    if(n == 0):
        return 0
    elif(n == 1):
        return 1
    else:
        while(i != n):
            c = a + b
            a = b 
            b = c
            i += 1 
    return c


def golden_phi(n: int) -> int:
    res = 1
    for i in range(1, n+1):
        res = 1 + 1 / res
    return res


def sqrt5(n: int) -> int:
    return n **(0.2)


def pow(a: float, n: int) -> float:
    i = 0
    res = 1
    if(n == 0):
        return 1.0
    elif(n == 1):
        return a
    while(i != n):
        res = res * a
        i += 1
    return res
    
